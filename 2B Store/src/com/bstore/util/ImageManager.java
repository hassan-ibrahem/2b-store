package com.bstore.util;

import java.util.HashMap;
import java.util.Map;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;
import com.bstore.R;
import com.bstore.view.ApplicationClass;

public class ImageManager
{
	private HashMap<String, Bitmap> productsImages;
	private SparseArray<Bitmap> mBitmaps;
	private SparseArray<Drawable> mDrawables;
	private Context mContext;

	private boolean mActive = true;
	static ImageManager imageManager;

	public static ImageManager getInstance()
	{
		if (imageManager == null)
			imageManager = new ImageManager();
		return imageManager;
	}

	public ImageManager()
	{
		productsImages = new HashMap<String, Bitmap>();
		mBitmaps = new SparseArray<Bitmap>();
		mDrawables = new SparseArray<Drawable>();
		mContext = ApplicationClass.context;
	}

	public synchronized Bitmap getProductImage(String URL)
	{
		return productsImages.get(URL);
	}

	/**
	 * Put the contact image if not found, or update the current one.
	 * @param URL
	 * @param productImage
	 */
	public void putProductImage(String URL, Bitmap productImage)
	{
		if (mActive)
		{
			if (!productsImages.containsKey(URL))
			{
				productsImages.put(URL, productImage);
			}
			else
			{
				productsImages.remove(URL);
				productsImages.put(URL, productImage);
			}
		}
	}

	// We need to share and cache resources between objects to save on memory.
	public synchronized Bitmap getBitmap(int resource)
	{
		if (mActive)
		{
			Bitmap bitmap = mBitmaps.get(resource);
			if (bitmap == null)
			{
				bitmap = BitmapFactory.decodeResource(mContext.getResources(), resource);
				mBitmaps.put(resource, bitmap);
			}
			return bitmap;
		}
		return null;
	}

	public Drawable getDrawable(int resource)
	{
		if (mActive)
		{
			if (mDrawables.get(resource) == null)
			{
				mDrawables.put(resource, mContext.getResources().getDrawable(resource));
			}
			return mDrawables.get(resource);
		}
		return null;
	}

	/**
	 * clear images hash map.
	 */
	public void clearImages()
	{
		for (Map.Entry<String, Bitmap> entry : productsImages.entrySet())
		{
			if (entry.getValue() != null)
			{
				entry.getValue().recycle();
				entry.setValue(null);
			}
		}
		productsImages.clear();
	}

	public void recycleBitmaps()
	{
		int key = 0;
		for (int i = 0; i < mBitmaps.size(); i++)
		{
			key = mBitmaps.keyAt(i);
			// get the object by the key.
			Bitmap entry = mBitmaps.get(key);
			if (entry != null)
			{
				entry.recycle();
				entry = null;
			}
		}
		mBitmaps.clear();
	}

	public void unbindDrawable()
	{
		int key = 0;
		for (int i = 0; i < mDrawables.size(); i++)
		{
			key = mDrawables.keyAt(i);
			// get the object by the key.
			Drawable entry = mDrawables.get(key);
			if (entry != null)
				entry.setCallback(null);
		}
		mBitmaps.clear();
	}

	public ImageManager setActive(boolean b)
	{
		mActive = b;
		return this;
	}

	public boolean isActive()
	{
		return mActive;
	}
}

package com.bstore.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.util.Enumeration;
import com.bstore.view.ApplicationClass;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.util.Log;

public class ConnectionDetector
{

	public static boolean inProcess = false;

	/**
	 * Checking for all possible internet providers
	 **/
	public static boolean checkNetworkAvailability()
	{
		try
		{
			ConnectivityManager connectivity = (ConnectivityManager) ApplicationClass.context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null)
			{
				NetworkInfo info = connectivity.getActiveNetworkInfo();
				if (info != null && info.isConnectedOrConnecting()

				&& connectivity.getActiveNetworkInfo().isAvailable()

				&& connectivity.getActiveNetworkInfo().isConnected())
				{
					return true;
				}
			}
			return false;
		}
		catch (Exception exc)
		{
			return false;
		}
	}

	public static boolean isConnectingToInternet(Context context)
	{
		if (checkNetworkAvailability())
		{
			try
			{
				HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
				urlc.setRequestProperty("User-Agent", "Test");
				urlc.setRequestProperty("Connection", "close");
				urlc.setConnectTimeout(6000);
				urlc.connect();
				return (urlc.getResponseCode() == 200);
			}
			catch (IOException e)
			{
				Log.e(ApplicationClass.ApplicationTag, "No network available");
			}
		}
		else
		{
			Log.e(ApplicationClass.ApplicationTag, "No network available!");
		}
		return false;
	}

	static public String getLocalIpAddress()
	{
		try
		{
			for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();)
			{
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();)
				{
					InetAddress inetAddress = enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress())
					{
						return inetAddress.getHostAddress().toString();
					}
				}
			}
		}
		catch (SocketException ex)
		{
			Log.e(ApplicationClass.ApplicationTag, "Error while getting self IP", ex);
		}
		return null;
	}

	static public boolean getConnectionStatus()
	{
		if (inProcess == true)
			return false;
		else
			inProcess = true;
		ConnectivityManager connectivity = (ConnectivityManager) ApplicationClass.context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null)
		{
			NetworkInfo info = connectivity.getActiveNetworkInfo();
			int type = info.getType();
			// Wifi connected
			if (info.isConnected() && (type == ConnectivityManager.TYPE_WIFI || type == 9))
			{
				return isConnectingToInternet(ApplicationClass.context);
			}
			// Type 3,4,5 are other mobile data ways
			if (info.isConnected() && (type == ConnectivityManager.TYPE_MOBILE || (type <= 5 && type >= 3)))
			{
				int subType = info.getSubtype();
				// 3G (or better)
				if (subType >= TelephonyManager.NETWORK_TYPE_UMTS)
				{
					return true;
				}
				// GPRS (or unknown)
				if ((subType == TelephonyManager.NETWORK_TYPE_GPRS || subType == TelephonyManager.NETWORK_TYPE_UNKNOWN))
				{
					return true;
				}
				// EDGE
				if (subType == TelephonyManager.NETWORK_TYPE_EDGE)
				{
					return true;
				}
			}
		}
		return false;
	}
}

package com.bstore.model;

import java.util.List;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("category")
public class productsCategory
{
	public CategoryInfo category_info;
	public Products products;

	public productsCategory(CategoryInfo category_info, Products products)
	{
		this.category_info = category_info;
		this.products = products;
	}

	@XStreamAlias("category_info")
	public static class CategoryInfo
	{
		public String parent_title;
		public String parent_id;
		public String has_more_items;
	}

	@XStreamAlias("products")
	public static class Products
	{
		@XStreamImplicit(itemFieldName = "item")
		public List<Item> item;

		public Products(List<Item> items)
		{
			this.item = items;
		}

	}

	@XStreamAlias("item")
	public static class Item
	{
		public String entity_id;
		public String name;
		public String entity_type;
		public String short_description;
		public String description;
		public String link;
		public String icon;
		public String in_stock;
		public String is_salable;
		public String has_gallery;
		public String has_options;
		public String rating_summary;
		public String reviews_count;
		public Price price;

		public Item(String entity_id, String name, String entity_type, String short_description, String description, String link, String icon, String in_stock, String is_salable, String has_gallery, String has_options, String rating_summary, String reviews_count, Price price)
		{
			this.entity_id = entity_id;
			this.name = name;
			this.entity_type = entity_type;
			this.short_description = short_description;
			this.description = description;
			this.link = link;
			this.icon = icon;
			this.in_stock = in_stock;
			this.is_salable = is_salable;
			this.has_gallery = has_gallery;
			this.has_options = has_options;
			this.rating_summary = rating_summary;
			this.reviews_count = reviews_count;
			this.price = price;
		}

	}

	@XStreamAlias("price")
	public static class Price
	{
		@XStreamAsAttribute
		public String regular;
		@XStreamAsAttribute
		public String special;
	}
}

package com.bstore.model;

import java.util.List;
import com.bstore.model.Product.AdditinalAttributes;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("category")
public class SubCategories
{
	public CategoryInfo category_info;
	public Items items;
//	public Products products;

	public SubCategories(CategoryInfo categoryInfo, Items items)
	{
		this.category_info = categoryInfo;
		this.items = items;
	}

	@XStreamAlias("category_info")
	public static class CategoryInfo
	{
		public String parent_title;
		public String parent_id;
		public String has_more_items;

		public CategoryInfo(String parent_title, String parent_id, String has_more_items)
		{
			this.parent_title = parent_title;
			this.parent_id = parent_id;
			this.has_more_items = has_more_items;
		}

	}

	@XStreamAlias("items")
	public static class Items
	{
		@XStreamImplicit(itemFieldName = "item")
		public List<Item> items;

		public Items(List<Item> items)
		{
			this.items = items;
		}

		/**********************************************************************/
		@XStreamAlias("item")
		public static class Item
		{
			public String label;
			public String entity_id;
			public String content_type;
			public String parent_id;
			public String icon;

			public Item(String label, String entity_id, String content_type, String parent_id, String icon)
			{
				this.label = label;
				this.entity_id = entity_id;
				this.content_type = content_type;
				this.parent_id = parent_id;
				this.icon = icon;
			}
		}
	}

	@XStreamAlias("products")
	public static class Products
	{
		@XStreamImplicit(itemFieldName = "productItem")
		public List<Item> productItem;

		public Products(List<Item> item)
		{
			this.productItem = item;
		}

		/**********************************************************************/
		@XStreamAlias("productItem")
		public static class Item
		{
			public String entity_id;
			public String name;
			public String entity_type;
			public String short_description;
			public String description;
			public String link;
			public String icon;
			public String in_stock;
			public String is_salable;
			public String has_gallery;
			public String has_options;
			public String rating_summary;
			public String reviews_count;
			public String price;
			public AdditinalAttributes additional_attributes;

			public Item(String entity_id, String name, String entity_type, String short_description, String description, String link, String icon, String in_stock, String is_salable, String has_gallery, String has_options, String rating_summary, String reviews_count, String price, AdditinalAttributes additional_attributes)
			{
				this.entity_id = entity_id;
				this.name = name;
				this.entity_type = entity_type;
				this.short_description = short_description;
				this.description = description;
				this.link = link;
				this.icon = icon;
				this.in_stock = in_stock;
				this.is_salable = is_salable;
				this.has_gallery = has_gallery;
				this.has_options = has_options;
				this.rating_summary = rating_summary;
				this.reviews_count = reviews_count;
				this.price = price;
				this.additional_attributes = additional_attributes;
			}
		}
	}
}

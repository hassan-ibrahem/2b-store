package com.bstore.model;

import java.util.List;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("images")
public class ProductImages
{
	@XStreamImplicit(itemFieldName = "image")
	public List<Image> images;

	public ProductImages(List<Image> images)
	{
		this.images = images;
	}

	@XStreamAlias("image")
	public static class Image
	{
		@XStreamImplicit(itemFieldName = "file")
		public List<ImageFile> files;
	}

	@XStreamAlias("file")
	public static class ImageFile
	{
		@XStreamAsAttribute
		public String type;
		@XStreamAsAttribute
		public String url;
		@XStreamAsAttribute
		public String id;
		@XStreamAsAttribute
		public String modification_time;
	}
}

package com.bstore.model;

import android.content.ContentValues;

public class Category
{
	/**********************************/
	public static final String CATEGORIES_TABLE_NAME = "categories";

	public static final String FIELD_ID = "entity_id";
	public static final String FIELD_LABEL = "name";
	public static final String FIELD_CONTENT_TYPE = "content_type";
	public static final String FIELD_ICON = "icon";
	public static final String FIELD_MODIFICATION_TIME = "modification_time";

	/**********************************/

	public String entityId;
	public String name;
	public String contentType;
	public String icon;
	public long modificationTime;

	public Category()
	{

	}

	public Category(ContentValues cv)
	{
		String stringReader = null;
		long longValue;
		stringReader = cv.getAsString(FIELD_ID);
		if (stringReader != null)
			entityId = stringReader;

		stringReader = cv.getAsString(FIELD_LABEL);
		if (stringReader != null)
			entityId = stringReader;

		stringReader = cv.getAsString(FIELD_CONTENT_TYPE);
		if (stringReader != null)
			entityId = stringReader;

		stringReader = cv.getAsString(FIELD_ICON);
		if (stringReader != null)
			entityId = stringReader;

		longValue = cv.getAsLong(FIELD_MODIFICATION_TIME);
		modificationTime = longValue;
	}
}

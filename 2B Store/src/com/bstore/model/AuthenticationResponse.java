package com.bstore.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("message")
public class AuthenticationResponse
{
	public String status;
	public String no_changes;

}

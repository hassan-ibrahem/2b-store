package com.bstore.model;

import java.util.List;
import android.content.ContentValues;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("home")
public class HomeResponse
{
	public String home_banner;
	public Categories categories;

	public HomeResponse(String home_banner, Categories categories)
	{
		this.home_banner = home_banner;
		this.categories = categories;
	}

	/******************************************************************************/
	@XStreamAlias("categories")
	public static class Categories
	{
		@XStreamImplicit(itemFieldName = "item")
		public List<Item> items;
		public CategoryInfo category_info;

		public Categories(List<Item> items, CategoryInfo categoryInfo)
		{
			this.items = items;
			this.category_info = categoryInfo;
		}
	}
	
	/******************************************************************************/

	@XStreamAlias("category_info")
	public static class CategoryInfo
	{
		public String parent_title;
		public String parent_id;
		public String has_more_items;
	}

	
	/******************************************************************************/
	
	@XStreamAlias("item")
	public static class Item
	{
		public String label;
		public String entity_id;
		public String content_type;
		public String icon;
		public String parent_id;
		@XStreamAsAttribute
		public String modification_time;

		public Item(String label, String entity_id, String content_type, String icon, String modification_time)
		{
			this.label = label;
			this.entity_id = entity_id;
			this.content_type = content_type;
			this.icon = icon;
			this.modification_time = modification_time;
		}
	}

}

package com.bstore.model;

import java.util.List;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("product")
public class Product
{
	public String entity_id;
	public String name;
	public String entity_type;
	public String short_description;
	public String description;
	public String link;
	public String icon;
	public String in_stock;
	public String is_salable;
	public String has_gallery;
	public String has_options;
	public String rating_summary;
	public String reviews_count;
	public String related_products;
	public String product;
	public Price price;
	public AdditinalAttributes additional_attributes;

	
	public Product(String entity_id, String name, String entity_type, String short_description, String description, String link, String icon, String in_stock, String is_salable, String has_gallery, String has_options, String rating_summary, String reviews_count, Price price, AdditinalAttributes additional_attributes)
	{
		this.entity_id = entity_id;
		this.name = name;
		this.entity_type = entity_type;
		this.short_description = short_description;
		this.description = description;
		this.link = link;
		this.icon = icon;
		this.in_stock = in_stock;
		this.is_salable = is_salable;
		this.has_gallery = has_gallery;
		this.has_options = has_options;
		this.rating_summary = rating_summary;
		this.reviews_count = reviews_count;
		this.price = price;
		this.additional_attributes = additional_attributes;
	}
	@XStreamAlias("price")
	public static class Price
	{
		@XStreamAsAttribute
		public String regular;
		@XStreamAsAttribute
		public String special;
	}
	
	/**
	 * @author Hassan
	 */
	@XStreamAlias("additional_attributes")
	public static class AdditinalAttributes
	{
		@XStreamImplicit(itemFieldName = "item")
		public List<Item> items;

		public AdditinalAttributes(List<Item> items)
		{
			this.items = items;
		}

		/**
		 * @author Hassan
		 */
		@XStreamAlias("item")
		public static class Item
		{
			public String label;
			public String value;

			public Item(String label, String value)
			{
				this.label = label;
				this.value = value;
			}

		}

	}

}

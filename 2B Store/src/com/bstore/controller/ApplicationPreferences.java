package com.bstore.controller;

import com.bstore.view.ApplicationClass;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class ApplicationPreferences
{
	SharedPreferences prefs;
	static ApplicationPreferences applicationPreferences;

	public static ApplicationPreferences getInstance()
	{
		if (applicationPreferences == null)
			applicationPreferences = new ApplicationPreferences();
		return applicationPreferences;
	}

	public ApplicationPreferences()
	{
		prefs = PreferenceManager.getDefaultSharedPreferences(ApplicationClass.context);
	}

	public void setNoChangesStatus(boolean noChangesStatus)
	{
		Editor edit = prefs.edit();
		edit.putBoolean("NoChangesStatus", noChangesStatus);
		edit.apply();
	}

	public boolean getNoChangesStatus()
	{
		boolean noChangesStatus = prefs.getBoolean("NoChangesStatus", false);
		return noChangesStatus;

	}
}

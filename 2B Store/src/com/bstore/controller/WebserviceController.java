package com.bstore.controller;

import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Random;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import com.bstore.model.AuthenticationResponse;
import com.bstore.model.Product;
import com.bstore.model.ProductImages;
import com.bstore.model.SubCategories;
import com.bstore.model.HomeResponse;
import com.bstore.model.WebserviceResponse;
import com.bstore.model.productsCategory;
import com.bstore.util.ImageManager;
import com.bstore.view.ApplicationClass;
import com.thoughtworks.xstream.XStream;

public class WebserviceController implements IwebseriveManager
{
	final private String ExceptionTag = "Exception, WebserviceController==> ";
	final private String INDEX_URL = "http://2bcomputer.com/xmlconnect/index";
	final private String SUB_CATEGORIES_URL = "http://www.2bcomputer.com//xmlconnect/catalog/category/id/";
	final private String PRODUCT_INFO_URL = "http://www.2bcomputer.com//xmlconnect/catalog/product/id/";
	final private String PRODUCT_GALLERY_URL = "http://www.2bcomputer.com//xmlconnect/catalog/productgallery/id/";
	final private String AUTHENTICATION_URL = "http://2bcomputer.com/xmlconnect/configuration/index/app_code/enand1";
	String frontEnd;
	public String extraNoCach;
	private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	WindowManager wm = (WindowManager) ApplicationClass.context.getSystemService(Context.WINDOW_SERVICE);
	Display display = wm.getDefaultDisplay();
	String screenSize = display.getWidth() + "x" + display.getHeight();
	public final static int AUTHENTICATION_REQUEST = 1;
	public final static int CATEGORIES_REQUEST = 2;
	boolean noChanges = false;
	public static WebserviceController webserviceController;

	private WebserviceController()
	{

	}

	public static WebserviceController getInstance()
	{
		if (webserviceController == null)
			webserviceController = new WebserviceController();
		return webserviceController;
	}

	/**
	 * Asking to register new account.
	 * @param mobileNumber
	 */
	public AuthenticationResponse Authenticate(Date updatedAt)
	{
		try
		{
			String extraParameters = "/updated_at/" + updatedAt.getTime() + "/screen_size/" + screenSize + "";
			Log.d(ApplicationClass.ApplicationTag, "Request==>" + AUTHENTICATION_URL + extraParameters);
			WebserviceResponse webserviceResponse = new WebserviceManager(AUTHENTICATION_URL + extraParameters, null).executeSynchronous();
			String response = webserviceResponse.responseMessage;
			frontEnd = webserviceResponse.frontEndKey;
			extraNoCach = webserviceResponse.extraNoCach;
			screenSize = webserviceResponse.screenSize;
			XStream xstream = new XStream();
			xstream.processAnnotations(AuthenticationResponse.class);
			AuthenticationResponse authenticationResponse = (AuthenticationResponse) xstream.fromXML(response);
			noChanges = authenticationResponse.no_changes.equals("1");
			return authenticationResponse;
		}
		catch (Exception exc)
		{
			Log.d(ApplicationClass.ApplicationTag, "Exception, WebserviceController =>", exc);
			return null;
		}
	}

	public HomeResponse requestCategories()
	{
		try
		{
			String parameters = "frontend=" + frontEnd + "; external_no_cache=" + extraNoCach + "; app_code=enand1; screen_size=" + screenSize;
			WebserviceResponse webserviceResponse = new WebserviceManager(INDEX_URL, parameters).executeSynchronous();
			XStream xstream = new XStream();
			xstream.processAnnotations(HomeResponse.class);
			HomeResponse homeResponse = (HomeResponse) xstream.fromXML(webserviceResponse.responseMessage);
			Log.d(ApplicationClass.ApplicationTag, "home_banner==>" + homeResponse.home_banner);
			return homeResponse;
		}
		catch (Exception exc)
		{
			Log.d(ApplicationClass.ApplicationTag, "Exception, WebserviceController =>", exc);
			return null;
		}
	}

	public SubCategories requestSubCategories(String categoryId)
	{
		try
		{
			String parameters = "frontend=" + frontEnd + "; external_no_cache=" + extraNoCach + "; app_code=enand1; screen_size=" + screenSize;
			WebserviceResponse webserviceResponse = new WebserviceManager(SUB_CATEGORIES_URL + categoryId, parameters).executeSynchronous();
			String[] old = webserviceResponse.responseMessage.split("<products>");
			String oldString = old[0];
			webserviceResponse.responseMessage = oldString;
			Log.d(ApplicationClass.ApplicationTag, "response is ==>" + webserviceResponse.responseMessage);
			XStream xstream = new XStream();
			xstream.processAnnotations(SubCategories.class);
			SubCategories categoryItems = (SubCategories) xstream.fromXML(webserviceResponse.responseMessage);
			if (categoryItems != null)
				Log.d(ApplicationClass.ApplicationTag, "Parsing ==>" + categoryItems.items.items.get(0).label);
			return categoryItems;
		}
		catch (Exception exc)
		{
			Log.d(ApplicationClass.ApplicationTag, "Exception, WebserviceController =>", exc);
			return null;
		}
	}

	public productsCategory requestProducts(String categoryId, String offest, String count)
	{
		try
		{
			String parameters = "frontend=" + frontEnd + "; external_no_cache=" + extraNoCach + "; app_code=enand1; screen_size=" + screenSize;
			WebserviceResponse webserviceResponse = new WebserviceManager(SUB_CATEGORIES_URL + categoryId + "/offset/" + offest + "/count/" + count, parameters).executeSynchronous();
			// webserviceResponse.responseMessage = webserviceResponse.responseMessage.replaceAll("<item", "<product");
			Log.d(ApplicationClass.ApplicationTag, "response is ==>" + webserviceResponse.responseMessage);
			XStream xstream = new XStream();
			xstream.processAnnotations(productsCategory.class);
			productsCategory productCategory = (productsCategory) xstream.fromXML(webserviceResponse.responseMessage);
			return productCategory;
		}
		catch (Exception exc)
		{
			Log.d(ApplicationClass.ApplicationTag, "Exception, WebserviceController =>", exc);
			return null;
		}
	}

	public Product requestProduct(String productId)
	{
		try
		{
			String parameters = "frontend=" + frontEnd + "; external_no_cache=" + extraNoCach + "; app_code=enand1; screen_size=" + screenSize;
			WebserviceResponse webserviceResponse = new WebserviceManager(PRODUCT_INFO_URL + productId, parameters).executeSynchronous();
			// webserviceResponse.responseMessage = webserviceResponse.responseMessage.replaceAll("<item", "<product");
			Log.d(ApplicationClass.ApplicationTag, "response is ==>" + webserviceResponse.responseMessage);
			XStream xstream = new XStream();
			xstream.processAnnotations(Product.class);
			Product product = (Product) xstream.fromXML(webserviceResponse.responseMessage);
			return product;
		}
		catch (Exception exc)
		{
			Log.d(ApplicationClass.ApplicationTag, "Exception, WebserviceController =>", exc);
			return null;
		}
	}

	public ProductImages requestProductGallery(String productId)
	{
		try
		{
			String parameters = "frontend=" + frontEnd + "; external_no_cache=" + extraNoCach + "; app_code=enand1; screen_size=" + screenSize;
			WebserviceResponse webserviceResponse = new WebserviceManager(PRODUCT_GALLERY_URL + productId, parameters).executeSynchronous();
			// webserviceResponse.responseMessage = webserviceResponse.responseMessage.replaceAll("<item", "<product");
			Log.d(ApplicationClass.ApplicationTag, "response is ==>" + webserviceResponse.responseMessage);
			XStream xstream = new XStream();
			xstream.processAnnotations(ProductImages.class);
			ProductImages productImages = (ProductImages) xstream.fromXML(webserviceResponse.responseMessage);
			return productImages;
		}
		catch (Exception exc)
		{
			Log.d(ApplicationClass.ApplicationTag, "Exception, WebserviceController =>", exc);
			return null;
		}
	}

	public Bitmap DownloadImage(String URL)
	{
		Bitmap bitmap = null;
		InputStream in = null;
		try
		{
			in = new WebserviceManager(URL, null).executeSynchronousHttpGet(URL);
			Log.d(ApplicationClass.ApplicationTag, " Open Connection Done ");
			bitmap = BitmapFactory.decodeStream(in);
			Log.d(ApplicationClass.ApplicationTag, " Set image bitmap Done ");
			in.close();
			ImageManager.getInstance().putProductImage(URL, bitmap);
		}
		catch (IOException e1)
		{
			Log.d(ApplicationClass.ApplicationTag, ExceptionTag, e1);
		}
		return bitmap;
	}

	@Override
	public void onFinish(int webserviceType, String response, Object extraFiled)
	{

	}

	public static String randomString(int len)
	{
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
		{
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		}
		return sb.toString();
	}

	public static String sha1(String input) throws NoSuchAlgorithmException
	{
		MessageDigest mDigest = MessageDigest.getInstance("SHA1");
		byte[] result = mDigest.digest(input.getBytes());
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < result.length; i++)
		{
			sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
		}

		return sb.toString();
	}

}

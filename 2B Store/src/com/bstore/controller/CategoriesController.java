package com.bstore.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;
import com.bstore.model.Category;
import com.bstore.model.HomeResponse;
import com.bstore.model.Product;
import com.bstore.model.ProductImages;
import com.bstore.model.SubCategories;
import com.bstore.model.SubCategories.Items.Item;
import com.bstore.model.productsCategory;
import com.bstore.util.ConnectionDetector;
import com.bstore.util.ImageManager;
import com.bstore.view.ApplicationClass;

public class CategoriesController
{
	private static CategoriesController categoriesController;

	public static CategoriesController getInstance()
	{
		if (categoriesController == null)
			categoriesController = new CategoriesController();
		return categoriesController;
	}

	private CategoriesController()
	{
		// TODO Auto-generated constructor stub
	}

	ArrayList<Category> categories = new ArrayList<Category>();

	public void setProductImage(final ImageView imageView, final Handler handler, final String URL)
	{
		Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				Log.d(ApplicationClass.ApplicationTag, "URL ===> " + URL);
				Bitmap productImage = ImageManager.getInstance().getProductImage(URL);
				if (productImage == null)
				{
					productImage = WebserviceController.getInstance().DownloadImage(URL);
				}
				if (productImage == null)
					return;
				final Bitmap bitmapProduct = productImage;
				handler.post(new Runnable()
				{
					@Override
					public void run()
					{
						imageView.setImageBitmap(bitmapProduct);
					}
				});
			}
		});
		thread.start();

	}

	public ArrayList<Category> getCategories()
	{
		ArrayList<Category> categories = new ArrayList<Category>();
		HomeResponse homeResponse = WebserviceController.getInstance().requestCategories();
		if (homeResponse == null)
			return null;
		for (HomeResponse.Item item : homeResponse.categories.items)
		{
			Category categoryItem = new Category();
			categoryItem.contentType = item.content_type;
			categoryItem.entityId = item.entity_id;
			categoryItem.icon = item.icon;
			if (item.modification_time != null)
				categoryItem.modificationTime = new Date(item.modification_time).getTime();
			categoryItem.name = item.label;
			categories.add(categoryItem);
		}
		this.categories = categories;
		return categories;
	}

	public List<Item> getSubCategories(String categoryId)
	{
		SubCategories categoryItems = WebserviceController.getInstance().requestSubCategories(categoryId);
		if (categoryItems == null)
			return null;
		return categoryItems.items.items;
	}

	public List<productsCategory.Item> getProducts(String categoryId)
	{
		productsCategory productCategory = WebserviceController.getInstance().requestProducts(categoryId, "0", "10");
		if (productCategory == null)
			return null;
		return productCategory.products.item;
	}

	public Product getProductInfo(String productId)
	{
		Product product = WebserviceController.getInstance().requestProduct(productId);
		if (product == null)
			return null;
		return product;
	}

	public List<ProductImages.Image> getProductGallery(String productId)
	{
		ProductImages images = WebserviceController.getInstance().requestProductGallery(productId);
		if (images == null)
			return null;
		return images.images;
	}

}

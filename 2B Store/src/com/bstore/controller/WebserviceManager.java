package com.bstore.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;
import com.bstore.model.WebserviceResponse;
import com.bstore.util.ConnectionDetector;
import com.bstore.view.ApplicationClass;

public class WebserviceManager
{
	public static final String ExceptionTag = "WebserviceManager , Exception==> ";
	private DefaultHttpClient client;
	private String URL;
	String paramters;
	int webserviceType;
	IwebseriveManager iWebserciceManager;
	Object extraField = null;

	public WebserviceManager(IwebseriveManager iWebserciceManager, int webserviceType, String URL, String paramters, Object extraField)
	{
		client = (DefaultHttpClient) getNewHttpClient();
		final HttpParams httpParameters = client.getParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, 120 * 1000);
		HttpConnectionParams.setSoTimeout(httpParameters, 120 * 1000);
		this.iWebserciceManager = iWebserciceManager;
		this.webserviceType = webserviceType;
		this.URL = URL;
		this.paramters = paramters;
		this.extraField = extraField;
	}

	public WebserviceManager(String URL, String paramters)
	{
		client = (DefaultHttpClient) getNewHttpClient();
		this.URL = URL;
		this.paramters = paramters;
	}

	/**
	 * Execute the webservice in the post method Asynchronously.
	 * @return string result
	 */
	public void executeAsynchronous()
	{
		Thread thread = new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				String responseMessage = "";
				HttpResponse response;
				try
				{
					HttpPost post = new HttpPost(URL);
					// post.setEntity(new UrlEncodedFormEntity(paramters));
					Log.d(ApplicationClass.ApplicationTag, "WebService Request ====> paramter " + " => " + URL);
					Log.d("kamal", "WebService Request ====> paramter " + " => " + URL);
					response = client.execute(post);
					/* Checking response */
					HttpEntity entityMsg = response.getEntity();
					responseMessage = EntityUtils.toString(entityMsg);
					Log.d(ApplicationClass.ApplicationTag, "WebService Response ====> " + responseMessage);
					iWebserciceManager.onFinish(webserviceType, responseMessage, extraField);
				}
				catch (Exception e)
				{
					Log.d(ApplicationClass.ApplicationTag, ExceptionTag, e);
				}
			}
		});
		thread.start();
	}

	public InputStream executeSynchronousHttpGet(String urlString) throws IOException
	{
		InputStream inputStream = null;
		int response = -1;
		URL url = new URL(urlString.replaceAll("https", "http"));
		URLConnection conn = url.openConnection();
		try
		{
			HttpURLConnection httpConn = (HttpURLConnection) conn;
			httpConn.setAllowUserInteraction(false);
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setRequestMethod("GET");
			httpConn.connect();
			response = httpConn.getResponseCode();
			Log.d("kamal", "respooooonseCoooooode of download ==> " + response);
			if (response == HttpURLConnection.HTTP_OK)
			{
				inputStream = httpConn.getInputStream();
			}
		}
		catch (Exception ex)
		{
			Log.d(ApplicationClass.ApplicationTag, ExceptionTag, ex);
		}
		return inputStream;
	}

	/**
	 * Execute the webservice in the post method synchronously.
	 * @return string result
	 */
	public WebserviceResponse executeSynchronous()
	{
		String responseMessage = "";
		HttpResponse response;
		WebserviceResponse webserviceResponse = null;
		if (ConnectionDetector.checkNetworkAvailability())
		{
			try
			{
				HttpPost post = new HttpPost(URL);
				if (paramters != null)
					post.addHeader("Cookie", paramters);
				response = client.execute(post);
				/* Checking response */
				HttpEntity entityMsg = response.getEntity();
				List<Cookie> cookies = client.getCookieStore().getCookies();
				responseMessage = EntityUtils.toString(entityMsg);
				webserviceResponse = new WebserviceResponse();
				webserviceResponse.responseMessage = responseMessage;
				if (webserviceType == WebserviceController.AUTHENTICATION_REQUEST)
				{
					for (int i = 0; i < cookies.size(); i++)
					{
						if (cookies.get(i).getName().equals("frontend"))
						{
							webserviceResponse.frontEndKey = cookies.get(i).getValue();
						}
						else if (cookies.get(i).getName().equals("external_no_cache"))
						{
							webserviceResponse.extraNoCach = cookies.get(i).getValue();
						}
						else if (cookies.get(i).getName().equals("screen_size"))
						{
							webserviceResponse.screenSize = cookies.get(i).getValue();
						}
					}
				}
			}
			catch (Exception e)
			{

			}
		}
		return webserviceResponse;
	}

	// Trusting all certificates using HttpClient over HTTPS
	public static class MySSLSocketFactory extends SSLSocketFactory
	{
		SSLContext sslContext = SSLContext.getInstance("TLS");

		public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException
		{
			super(truststore);

			TrustManager tm = new X509TrustManager()
			{
				public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException
				{
				}

				public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException
				{
				}

				public X509Certificate[] getAcceptedIssuers()
				{
					return null;
				}
			};

			sslContext.init(null, new TrustManager[] { tm }, null);
		}

		@Override
		public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException
		{
			return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
		}

		@Override
		public Socket createSocket() throws IOException
		{
			return sslContext.getSocketFactory().createSocket();
		}
	}

	public HttpClient getNewHttpClient()
	{
		try
		{
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			SSLSocketFactory sSLSocketFactory = new MySSLSocketFactory(trustStore);
			sSLSocketFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			HttpParams params = new BasicHttpParams();
			params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			params.setParameter(CoreProtocolPNames.HTTP_CONTENT_CHARSET, HTTP.UTF_8);
			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			registry.register(new Scheme("https", sSLSocketFactory, 443));

			ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);
			HttpClient client = new DefaultHttpClient(ccm, params);
			return client;

		}
		catch (Exception e)
		{
			return new DefaultHttpClient();
		}
	}
}

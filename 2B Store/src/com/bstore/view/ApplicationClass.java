package com.bstore.view;

import java.util.Date;
import android.app.Application;
import android.content.Context;
import com.bstore.controller.WebserviceController;
import com.bstore.model.AuthenticationResponse;

public class ApplicationClass extends Application
{
	public final static String ApplicationTag = "com.2BStore";
	public static Context context;
	public static boolean authenticated = false;

	@Override
	public void onCreate()
	{
		super.onCreate();
		context = this;
	}
}

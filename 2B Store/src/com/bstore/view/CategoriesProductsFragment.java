package com.bstore.view;

import java.util.Date;
import java.util.List;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.bstore.R;
import com.bstore.controller.CategoriesController;
import com.bstore.controller.WebserviceController;
import com.bstore.model.AuthenticationResponse;
import com.bstore.model.productsCategory;

public class CategoriesProductsFragment extends SherlockFragmentActivity implements OnClickListener, OnItemClickListener
{
	Handler mHandler;
	private ListView listView;
	private ProgressBar prgressBar;
	private CategoriesProductsAdapter mAdapter;
	String categoryId;
	Button refreshButton;
	LinearLayout networkStatusLayout;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.category_items);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		mHandler = new Handler();
		categoryId = getIntent().getStringExtra("categoryId");
		setTitle(getIntent().getStringExtra("title"));
		listView = (ListView) findViewById(R.id.listView);
		prgressBar = (ProgressBar) findViewById(R.id.progressBar);
		networkStatusLayout = (LinearLayout) findViewById(R.id.networkStatusLayout);
		refreshButton = (Button) findViewById(R.id.refreshButton);
		refreshButton.setOnClickListener(this);
		mAdapter = new CategoriesProductsAdapter(this, this, R.layout.product_item);
		listView.setAdapter(mAdapter);
		listView.setOnItemClickListener(this);
		setAdapter();
	}

	public void setAdapter()
	{
		Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				if (!ApplicationClass.authenticated)
				{
					final AuthenticationResponse response = WebserviceController.getInstance().Authenticate(new Date());
					mHandler.post(new Runnable()
					{
						@Override
						public void run()
						{
							if (response != null && response.status.equals("success"))
							{
								ApplicationClass.authenticated = true;
							}
							else
							{
								networkStatusLayout.setVisibility(View.VISIBLE);
								prgressBar.setVisibility(View.GONE);
							}
						}
					});
				}
				final List<productsCategory.Item> items = CategoriesController.getInstance().getProducts(categoryId);
				mHandler.post(new Runnable()
				{
					@Override
					public void run()
					{
						if (items == null)
						{
							networkStatusLayout.setVisibility(View.VISIBLE);
						}
						else
						{
							listView.setVisibility(View.VISIBLE);
							mAdapter.setData(items);
						}
						prgressBar.setVisibility(View.GONE);
					}
				});
			}
		});
		thread.start();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() == android.R.id.home)
		{
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v)
	{
		networkStatusLayout.setVisibility(View.GONE);
		prgressBar.setVisibility(View.VISIBLE);
		setAdapter();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		productsCategory.Item item = mAdapter.getItem(position);
		if (item.entity_type.equals("products"))
		{
			Intent intent = new Intent(this, CategoriesProductsFragment.class);
			intent.putExtra("categoryId", item.entity_id);
			startActivity(intent);
		}
		else if (item.entity_type.equals("simple"))
		{
			Intent intent = new Intent(this, ProductInfoFragment.class);
			intent.putExtra("productId", item.entity_id);
			startActivity(intent);
		}
	}
}

package com.bstore.view;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bstore.R;
import com.bstore.controller.CategoriesController;
import com.bstore.controller.WebserviceController;
import com.bstore.model.Category;
import com.bstore.util.ImageManager;

class CategoriesAdapter extends ArrayAdapter<Category>
{

	private final LayoutInflater inflater;
	private final Resources res;
	private final int itemLayout;
	final Handler handler = new Handler(Looper.getMainLooper());
	WeakReference<CategoriesFragment> categoriesFragment;

	static class CategoryViewHolder
	{
		final TextView categoryTextView;
		final ImageView categoryImageView;

		CategoryViewHolder(View view)
		{
			categoryTextView = (TextView) view.findViewById(R.id.categoryTextView);
			categoryImageView = (ImageView) view.findViewById(R.id.categoryImageView);
		}
	}

	public CategoriesAdapter(Context context, CategoriesFragment categoryFragment, int itemLayout)
	{
		// super(context, itemLayout, R.id.text, ListModel.getModel());
		super(context, itemLayout);
		inflater = LayoutInflater.from(context);
		res = context.getResources();
		this.itemLayout = itemLayout;
		this.categoriesFragment = new WeakReference<CategoriesFragment>(categoryFragment);
	}

	public void setData(ArrayList<Category> categories)
	{
		clear();
		for (Category category : categories)
		{
			add(category);
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		CategoryViewHolder holder;
		if (convertView == null)
		{
			convertView = inflater.inflate(itemLayout, null);
			holder = new CategoryViewHolder(convertView);
			convertView.setTag(holder);
		}
		else
		{
			holder = (CategoryViewHolder) convertView.getTag();
		}
		holder.categoryImageView.setTag(position);
		Category category = getItem(position);
		holder.categoryTextView.setText(category.name);
		CategoriesController.getInstance().setProductImage(holder.categoryImageView, handler, category.icon);
		return convertView;
	}

}

/*******************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bstore.view;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import uk.co.senab.photoview.PhotoView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Window;
import com.bstore.R;
import com.bstore.controller.CategoriesController;
import com.bstore.controller.WebserviceController;
import com.bstore.model.ProductImages;
import com.bstore.model.ProductImages.Image;
import com.bstore.util.HackyViewPager;
import com.bstore.util.ImageManager;

public class ViewPagerActivity extends SherlockFragmentActivity
{
	public final static Handler Handler = new Handler();
	static int index;
	HackyViewPager mViewPager;
	static List<ProductImages.Image> images;
	LinearLayout progressForm;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_view_pager);
		mViewPager = (HackyViewPager) findViewById(R.id.view_pager);
		progressForm = (LinearLayout) findViewById(R.id.progressForm);
		setAdapter(getIntent().getStringExtra("productId"));
	}

	public void setAdapter(final String productId)
	{
		Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				images = CategoriesController.getInstance().getProductGallery(productId);
				for (Image image : images)
				{
					String url = image.files.get(1).url;
					ImageManager.getInstance().putProductImage(url, WebserviceController.getInstance().DownloadImage(url));
				}
				Handler.post(new Runnable()
				{
					@Override
					public void run()
					{
						progressForm.setVisibility(View.GONE);
						mViewPager.setVisibility(View.VISIBLE);
						mViewPager.setAdapter(new SamplePagerAdapter());
					}
				});
			}
		});
		thread.start();
	}

	static class SamplePagerAdapter extends PagerAdapter
	{

		@Override
		public int getCount()
		{
			return images.size();
		}

		@Override
		public View instantiateItem(ViewGroup container, int position)
		{
			PhotoView photoView = new PhotoView(container.getContext());
			// photoView.setImageResource(sDrawables[position]);
			// photoView.setc
			ProductImages.Image image = images.get(position);
			Bitmap bitmap = ImageManager.getInstance().getProductImage((image.files.get(1).url));
			if (bitmap != null)
				photoView.setImageBitmap(bitmap);
			CategoriesController.getInstance().setProductImage(photoView, Handler, image.files.get(0).url);
			// Now just add PhotoView to ViewPager and return it
			container.addView(photoView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			return photoView;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object)
		{
			container.removeView((View) object);
		}

		@Override
		public boolean isViewFromObject(View view, Object object)
		{
			return view == object;
		}

	}

	// @Override
	// public boolean onTouch(View v, MotionEvent event)
	// {
	// // if(photoView.getScale()>1.0f)
	// return false;
	// }

}

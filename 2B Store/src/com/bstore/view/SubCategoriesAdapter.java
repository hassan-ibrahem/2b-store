package com.bstore.view;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.bstore.R;
import com.bstore.model.SubCategories.Items.Item;

public class SubCategoriesAdapter extends ArrayAdapter<Item>
{

	private final LayoutInflater inflater;
	private final int itemLayout;
	final Handler handler = new Handler(Looper.getMainLooper());
	WeakReference<SubCategoriesFragment> categoryItemsFragment;

	static class CategoryViewHolder
	{
		final TextView categoryTextView;

		CategoryViewHolder(View view)
		{
			categoryTextView = (TextView) view.findViewById(R.id.categoryTextView);
		}
	}

	public SubCategoriesAdapter(Context context, SubCategoriesFragment categoryItemsFragment, int itemLayout)
	{
		super(context, itemLayout);
		inflater = LayoutInflater.from(context);
		this.itemLayout = itemLayout;
		this.categoryItemsFragment = new WeakReference<SubCategoriesFragment>(categoryItemsFragment);
	}

	public void setData(List<Item> categories)
	{
		clear();
		for (Item item : categories)
		{
			add(item);
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		CategoryViewHolder holder;
		if (convertView == null)
		{
			convertView = inflater.inflate(itemLayout, null);
			holder = new CategoryViewHolder(convertView);
			convertView.setTag(holder);
		}
		else
		{
			holder = (CategoryViewHolder) convertView.getTag();
		}
		Item item = getItem(position);
		holder.categoryTextView.setText(item.label);
		return convertView;
	}
}

package com.bstore.view;

import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.List;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NavUtils;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.bstore.R;
import com.bstore.controller.CategoriesController;
import com.bstore.controller.WebserviceController;
import com.bstore.model.AuthenticationResponse;
import com.bstore.model.Product;
import com.bstore.model.Product.AdditinalAttributes.Item;

public class ProductInfoFragment extends SherlockFragmentActivity implements OnClickListener
{
	LinearLayout loadingForm;
	View productInfoView;
	ImageView productImage;
	TextView productName;
	TextView regularPriceLabel;
	TextView regularPriceValue;
	View dashLine;
	TextView specialPriceLabel;
	TextView specialPriceValue;
	TextView inStock;
	TextView productShortDescription;
	TextView productDescription;
	ListView additinalAttributes;
	Handler mHandler;
	ProductInfoAdapter mAdapter;
	Product product;
	Button refreshButton;
	LinearLayout networkStatusLayout;
	String productId;

	@Override
	protected void onCreate(Bundle arg0)
	{
		super.onCreate(arg0);
		setContentView(R.layout.product_activity);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		mHandler = new Handler();
		initializeViews();
		mAdapter = new ProductInfoAdapter(this, this, R.layout.additinal_attributes_item);
		additinalAttributes.setAdapter(mAdapter);
		productId = getIntent().getStringExtra("productId");
		getProductInfo(productId);
	}

	public void initializeViews()
	{
		loadingForm = (LinearLayout) findViewById(R.id.progressForm);
		additinalAttributes = (ListView) findViewById(R.id.additinalAttributesListView);
		networkStatusLayout = (LinearLayout) findViewById(R.id.networkStatusLayout);
		refreshButton = (Button) findViewById(R.id.refreshButton);
		refreshButton.setOnClickListener(this);
		LayoutInflater inflater = getLayoutInflater();
		productInfoView = inflater.inflate(R.layout.product_info_header, null);
		productImage = (ImageView) productInfoView.findViewById(R.id.productImage);
		productImage.setOnClickListener(this);
		productName = (TextView) productInfoView.findViewById(R.id.productName);
		regularPriceLabel = (TextView) productInfoView.findViewById(R.id.regularPriceLabel);
		regularPriceValue = (TextView) productInfoView.findViewById(R.id.regularPriceValue);
		dashLine = (View) productInfoView.findViewById(R.id.line);
		specialPriceLabel = (TextView) productInfoView.findViewById(R.id.specialPriceLabel);
		specialPriceValue = (TextView) productInfoView.findViewById(R.id.specialPriceValue);
		inStock = (TextView) productInfoView.findViewById(R.id.inStock);
		productShortDescription = (TextView) productInfoView.findViewById(R.id.shortDescription);
		productDescription = (TextView) productInfoView.findViewById(R.id.description);

		additinalAttributes.addHeaderView(productInfoView);
		
	}

	public void getProductInfo(final String productId)
	{
		Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				if (!ApplicationClass.authenticated)
				{
					final AuthenticationResponse response = WebserviceController.getInstance().Authenticate(new Date());
					mHandler.post(new Runnable()
					{
						@Override
						public void run()
						{
							if (response != null && response.status.equals("success"))
							{
								ApplicationClass.authenticated = true;
							}
							else
							{
								networkStatusLayout.setVisibility(View.VISIBLE);
								loadingForm.setVisibility(View.GONE);
							}
						}
					});
				}
				product = CategoriesController.getInstance().getProductInfo(productId);
				mHandler.post(new Runnable()
				{
					@Override
					public void run()
					{
						if (product == null)
						{
							networkStatusLayout.setVisibility(View.VISIBLE);
						}
						else
						{
							additinalAttributes.setVisibility(View.VISIBLE);
							CategoriesController.getInstance().setProductImage(productImage, mHandler, product.icon);
							productName.setText(product.name);
							if (product.price.special != null)
							{
								dashLine.setVisibility(View.VISIBLE);
								specialPriceValue.setText(product.price.special);
							}
							else
							{
								dashLine.setVisibility(View.GONE);
								regularPriceLabel.setText("Price:");
								specialPriceLabel.setVisibility(View.GONE);
								specialPriceValue.setVisibility(View.GONE);
							}
							regularPriceValue.setText(product.price.regular);
							regularPriceLabel.setVisibility(View.VISIBLE);
							if (product.in_stock.equals("1"))
							{
								inStock.setText("In Stock");
								inStock.setTextColor(0xff217901);
							}
							else
							{
								inStock.setText("Out of Stock");
								inStock.setTextColor(0xffB00000);
							}
							productShortDescription.setText(product.short_description);
							productDescription.setText(Html.fromHtml(product.description), BufferType.SPANNABLE);
							mAdapter.setData(product.additional_attributes.items);
						}
						loadingForm.setVisibility(View.GONE);
					}
				});
			}
		});
		thread.start();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() == android.R.id.home)
		{
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public static class ProductInfoAdapter extends ArrayAdapter<Item>
	{

		private final LayoutInflater inflater;
		private final int itemLayout;
		final Handler handler = new Handler(Looper.getMainLooper());
		WeakReference<ProductInfoFragment> productInfoFragment;

		static class CategoriesProductsViewHolder
		{
			final TextView label;
			final TextView value;

			CategoriesProductsViewHolder(View view)
			{
				label = (TextView) view.findViewById(R.id.additinal_label);
				value = (TextView) view.findViewById(R.id.additinal_value);
			}
		}

		public ProductInfoAdapter(Context context, ProductInfoFragment productInfoFragment, int itemLayout)
		{
			super(context, itemLayout);
			inflater = LayoutInflater.from(context);
			this.itemLayout = itemLayout;
			this.productInfoFragment = new WeakReference<ProductInfoFragment>(productInfoFragment);
		}

		public void setData(List<Item> items)
		{
			clear();
			for (Item item : items)
			{
				add(item);
			}
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			CategoriesProductsViewHolder holder;
			if (convertView == null)
			{
				convertView = inflater.inflate(itemLayout, null);
				holder = new CategoriesProductsViewHolder(convertView);
				convertView.setTag(holder);
			}
			else
			{
				holder = (CategoriesProductsViewHolder) convertView.getTag();
			}
			Item item = getItem(position);
			if (!item.label.equals("Other"))
			{
				holder.label.setText(item.label + ":");
				holder.value.setText(item.value);
			}
			return convertView;
		}

	}

	@Override
	public void onClick(View v)
	{
		if (v.getId() == R.id.refreshButton)
		{
			networkStatusLayout.setVisibility(View.GONE);
			loadingForm.setVisibility(View.VISIBLE);
			getProductInfo(productId);
		}

		else if (product.has_gallery.equals("1"))
		{
			Intent intent = new Intent(this, ViewPagerActivity.class);
			intent.putExtra("productId", product.entity_id);
			startActivity(intent);
		}
	}

}

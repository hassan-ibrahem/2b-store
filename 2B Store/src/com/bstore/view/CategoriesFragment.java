package com.bstore.view;

import java.util.ArrayList;
import java.util.Date;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.bstore.R;
import com.bstore.controller.CategoriesController;
import com.bstore.controller.WebserviceController;
import com.bstore.model.AuthenticationResponse;
import com.bstore.model.Category;
import com.twotoasters.jazzylistview.JazzyGridView;
import com.twotoasters.jazzylistview.JazzyHelper;

public class CategoriesFragment extends SherlockFragment implements OnItemClickListener, OnClickListener
{

	Handler mHandler;
	private JazzyGridView mGrid;
	private ProgressBar prgressBar;
	private int mCurrentTransitionEffect = JazzyHelper.GROW;
	private CategoriesAdapter mAdapter;
	Button refreshButton;
	LinearLayout networkStatusLayout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.activity_grid, container, false);
		mHandler = new Handler();
		mGrid = (JazzyGridView) rootView.findViewById(android.R.id.list);
		prgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
		networkStatusLayout = (LinearLayout) rootView.findViewById(R.id.networkStatusLayout);
		refreshButton = (Button) rootView.findViewById(R.id.refreshButton);
		refreshButton.setOnClickListener(this);
		mAdapter = new CategoriesAdapter(getActivity(), this, R.layout.grid_item);
		mGrid.setAdapter(mAdapter);
		mGrid.setOnItemClickListener(this);
		setAdapter();
		setupJazziness(mCurrentTransitionEffect);
		return rootView;
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
	}

	private void setupJazziness(int effect)
	{
		mCurrentTransitionEffect = effect;
		mGrid.setTransitionEffect(mCurrentTransitionEffect);
	}

	public void setAdapter()
	{
		Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				if (!ApplicationClass.authenticated)
				{
					final AuthenticationResponse response = WebserviceController.getInstance().Authenticate(new Date());
					mHandler.post(new Runnable()
					{
						@Override
						public void run()
						{
							if (response != null && response.status.equals("success"))
							{
								ApplicationClass.authenticated = true;
							}
							else
							{
								networkStatusLayout.setVisibility(View.VISIBLE);
								prgressBar.setVisibility(View.GONE);
							}
						}
					});
				}
				final ArrayList<Category> categories = CategoriesController.getInstance().getCategories();

				mHandler.post(new Runnable()
				{
					@Override
					public void run()
					{
						if (categories == null)
						{
							networkStatusLayout.setVisibility(View.VISIBLE);
						}
						else
						{
							mGrid.setVisibility(View.VISIBLE);
							mAdapter.setData(categories);
						}
						prgressBar.setVisibility(View.GONE);
					}
				});
			}
		});
		thread.start();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		Category category = mAdapter.getItem(position);
		Intent intent = new Intent(getActivity(), SubCategoriesFragment.class);
		intent.putExtra("categoryId", category.entityId);
		intent.putExtra("title", category.name);
		startActivity(intent);
	}

	@Override
	public void onClick(View v)
	{
		networkStatusLayout.setVisibility(View.GONE);
		prgressBar.setVisibility(View.VISIBLE);
		setAdapter();
	}
}

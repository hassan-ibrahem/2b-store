package com.bstore.view;

import java.lang.ref.WeakReference;
import java.util.List;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bstore.R;
import com.bstore.controller.CategoriesController;
import com.bstore.controller.WebserviceController;
import com.bstore.model.productsCategory.Item;
import com.bstore.util.ImageManager;

public class CategoriesProductsAdapter extends ArrayAdapter<Item>
{

	private final LayoutInflater inflater;
	private final int itemLayout;
	final Handler handler = new Handler(Looper.getMainLooper());
	WeakReference<CategoriesProductsFragment> categoriesProductsFragment;

	static class CategoriesProductsViewHolder
	{
		final TextView productLabel;
		final TextView oldPrice;
		final View dash;
		final TextView currentPrice;
		final TextView inStock;
		final ImageView productImage;

		CategoriesProductsViewHolder(View view)
		{
			productLabel = (TextView) view.findViewById(R.id.productLabel);
			oldPrice = (TextView) view.findViewById(R.id.oldPrice);
			dash = (View) view.findViewById(R.id.line);
			currentPrice = (TextView) view.findViewById(R.id.currentPrice);
			inStock = (TextView) view.findViewById(R.id.inStock);
			productImage = (ImageView) view.findViewById(R.id.productImage);
		}
	}

	public CategoriesProductsAdapter(Context context, CategoriesProductsFragment categoriesProductsFragment, int itemLayout)
	{
		super(context, itemLayout);
		inflater = LayoutInflater.from(context);
		this.itemLayout = itemLayout;
		this.categoriesProductsFragment = new WeakReference<CategoriesProductsFragment>(categoriesProductsFragment);
	}

	public void setData(List<Item> categories)
	{
		clear();
		for (Item item : categories)
		{
			add(item);
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		CategoriesProductsViewHolder holder;
		if (convertView == null)
		{
			convertView = inflater.inflate(itemLayout, null);
			holder = new CategoriesProductsViewHolder(convertView);
			convertView.setTag(holder);
		}
		else
		{
			holder = (CategoriesProductsViewHolder) convertView.getTag();
		}
		Item item = getItem(position);
		holder.productLabel.setText(item.name);
		if (item.price.regular != null)
			holder.oldPrice.setText(item.price.regular);
		if (item.price.special != null)
		{
			holder.currentPrice.setText(item.price.special);
			holder.currentPrice.setVisibility(View.VISIBLE);
			holder.dash.setVisibility(View.VISIBLE);
		}
		else
		{
			holder.currentPrice.setVisibility(View.GONE);
			holder.dash.setVisibility(View.GONE);
		}
		if (item.in_stock.equals("1"))
		{
			holder.inStock.setText("In Stock");
			holder.inStock.setTextColor(0xff217901);
		}
		else
		{
			holder.inStock.setText("Out of Stock");
			holder.inStock.setTextColor(0xffB00000);
		}
		CategoriesController.getInstance().setProductImage(holder.productImage, handler, item.icon);
		return convertView;
	}

}

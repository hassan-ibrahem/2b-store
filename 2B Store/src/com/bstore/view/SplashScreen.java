package com.bstore.view;

import java.util.Date;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Window;
import com.bstore.R;
import com.bstore.controller.WebserviceController;
import com.bstore.model.AuthenticationResponse;

public class SplashScreen extends SherlockFragmentActivity implements OnClickListener
{
	boolean connecting = false;
	LinearLayout networkStatus;
	Button refreshButton;
	RelativeLayout splashScreenView;
	Handler handler;
	ImageView cartLogo;
	Animation rightLeftAnim;

	@Override
	protected void onCreate(Bundle arg0)
	{
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.splash_screen_activity);
		handler = new Handler();
		splashScreenView = (RelativeLayout) findViewById(R.id.splashScreenView);
		networkStatus = (LinearLayout) findViewById(R.id.networkStatusLayout);
		refreshButton = (Button) findViewById(R.id.refreshButton);
		cartLogo = (ImageView) findViewById(R.id.cardLogo);
		rightLeftAnim = AnimationUtils.loadAnimation(this, R.anim.right_left_anim);
		cartLogo.startAnimation(rightLeftAnim);
		refreshButton.setOnClickListener(this);
		try
		{
			authenticate();
			handler.postDelayed(new Runnable()
			{
				@Override
				public void run()
				{
					while (connecting == true)
					{
						try
						{
							Thread.sleep(200);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
					if (ApplicationClass.authenticated)
					{
						Intent intent = new Intent(SplashScreen.this, Home.class);
						startActivity(intent);
						finish();
					}
				}
			}, 8000);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void authenticate()
	{
		Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				connecting = true;
				AuthenticationResponse response = WebserviceController.getInstance().Authenticate(new Date());
				if (response != null && response.status.equals("success"))
				{
					ApplicationClass.authenticated = true;
					connecting = false;
				}
				else
				{
					connecting = false;
					handler.post(new Runnable()
					{

						@Override
						public void run()
						{
							networkStatus.setVisibility(View.VISIBLE);
							splashScreenView.setVisibility(View.GONE);
						}
					});
				}
			}
		});
		thread.start();
	}

	@Override
	public void onClick(View v)
	{
		networkStatus.setVisibility(View.GONE);
		splashScreenView.setVisibility(View.VISIBLE);
		authenticate();
		handler.postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				while (connecting == true)
				{
					try
					{
						Thread.sleep(200);
					}
					catch (InterruptedException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (ApplicationClass.authenticated)
				{
					Intent intent = new Intent(SplashScreen.this, Home.class);
					startActivity(intent);
					finish();
				}
			}
		}, 8000);

	}
}
